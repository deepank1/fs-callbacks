let fs = require("fs");
const { dirname } = require("path");
let path = require("path")



function writeFileDeleteFile() {

    let dataFolder = "random_files"

    fs.mkdir(path.join(__dirname,dataFolder), {recursive:true}, (err) => {
        if (err) {
            console.error("directory creation Error", err)
        }
        else {
            console.log("data directory created")

            let files = Array(10).fill(0)
                .map((currentValue, currentIndex) => {
                    return `${currentIndex}.json`
                })

            const createFiles = (filesList) => {
                let numberOfCreatedFiles = 0
                let numberofCreatedError = 0

                for (let index = 0; index < filesList.length; index++) {

                    fs.writeFile(path.join(__dirname,dataFolder,filesList[index]), JSON.stringify({ "name": "Deepank", "company": "MB" }), (err) => {
                        if (err) {
                            console.error("File creation Error", err)
                            numberofCreatedError++
                        }
                        else {
                            console.log(`random${index} file created`)
                            numberOfCreatedFiles++
                        }
                        if (numberOfCreatedFiles + numberofCreatedError == filesList.length) {
                            console.log("All created or errored")
                            deleteFiles(files)

                        }


                    })
                }
            }
            const deleteFiles = (filesList) => {
                let numberOfDeletedFiles = 0
                let numberofDeletedError = 0

                for (let index = 0; index < filesList.length; index++) {

                    fs.unlink(path.join(__dirname,dataFolder,filesList[index]), (err) => {
                        if (err) {
                            console.error("File deletion Error", err)
                            numberofDeletedError++
                        }
                        else {
                            console.log(`random${index} file deleted`)
                            numberOfDeletedFiles++
                        }
                        if (numberOfDeletedFiles + numberofDeletedError == filesList.length) {
                            console.log("All deleted or errored")
                        }


                    })
                }
            }
            createFiles(files)
        }
    })
}

module.exports = writeFileDeleteFile