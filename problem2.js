let fs = require("fs")
let path = require("path")

function fileDataManipulation() {

    let sampleFile = "lipsum.txt"
    let fileOfnewFilesPath = "filenames.txt"

    fs.readFile(path.join(__dirname,sampleFile), (err, data) => {
        if (err) {
            console.error("Reading sample file Error", err)
        }
        else {
            let upperFileName = "upper.js"
            fs.writeFile(path.join(__dirname,upperFileName), data.toString().toUpperCase(), (err) => {
                if (err) {
                    console.log("UpperFile creation Error", err)
                }
                else {
                    console.log("UpperFile created")
                    fs.writeFile(path.join(__dirname,fileOfnewFilesPath), upperFileName + "\n", (err) => {
                        if (err) {
                            console.log("UpperFile Path storing Error", err)
                        }
                        else {
                            console.log("UpperFile path stored successfully")
                            fs.readFile(path.join(__dirname,upperFileName), (err, data) => {
                                if (err) {
                                    console.error("Reading upperfile Error", err)
                                }
                                else {
                                    console.log("Reading upperFile successfully")
                                    let lowerContent = data.toString().toLowerCase().split(".")
                                    let lowerFileName = "lower.js"
                                    fs.writeFile(path.join(__dirname,lowerFileName), lowerContent.join(), (err) => {
                                        if (err) {
                                            console.error("lowerFile writing Error", err)
                                        }
                                        else {
                                            console.log("LowerFile created successfully")
                                            fs.appendFile(path.join(__dirname,fileOfnewFilesPath), lowerFileName+"\n", (err) => {
                                                if (err) {
                                                    console.error("LowerFile Path storing Error", err)
                                                }
                                                else {
                                                    console.log("LowerFile Path stored successfully")
                                                    fs.readFile(path.join(__dirname,lowerFileName), (err, data) => {
                                                        if (err) {
                                                            console.error("LowerFile reading Error", err)
                                                        }
                                                        else {
                                                            console.log("LowerFile read successfully")
                                                            let sortedLowerFile = data.toString().split(" ").sort()
                                                            let sortedLowerFileName = "sortedLower.js"
                                                            fs.writeFile(path.join(__dirname,sortedLowerFileName), sortedLowerFile.join(" "), (err) => {
                                                                if (err) {
                                                                    console.error("Sorted lower file write Error", err)
                                                                }
                                                                else {
                                                                    console.log("sorted lower file write success")
                                                                    fs.appendFile(path.join(__dirname,fileOfnewFilesPath), sortedLowerFileName+"\n", (err) => {
                                                                        if (err) {
                                                                            console.error("sortedLower file path Error", err)
                                                                        }
                                                                        else {
                                                                            console.log("sorted Lower file path stored")
                                                                            fs.readFile(path.join(__dirname,upperFileName), (err, data) => {
                                                                                if (err) {
                                                                                    console.error("UpperFile reading Error", err)
                                                                                }
                                                                                else {
                                                                                    console.log("UpperFile read successfully")
                                                                                    let sortedUpperFile = data.toString().split(" ").sort()
                                                                                    let sortedUpperFileName = "sortedUpper.js"
                                                                                    fs.writeFile(path.join(__dirname,sortedUpperFileName), sortedUpperFile.join(" "), (err) => {
                                                                                        if (err) {
                                                                                            console.error("Sorted upperFile write Error",err)
                                                                                        }
                                                                                        else{
                                                                                            console.log("Sorted upperFile write successfully")
                                                                                            fs.appendFile(path.join(__dirname,fileOfnewFilesPath),sortedUpperFileName, (err)=>{
                                                                                                if(err){
                                                                                                    console.error("sorted upperFile path Error",err)
                                                                                                }
                                                                                                else{
                                                                                                    console.log("sorted upperFile path stored")
                                                                                                    fs.readFile(path.join(__dirname,fileOfnewFilesPath),(err,data)=>{
                                                                                                        if(err){
                                                                                                            console.error("filenames.txt read Error",err)
                                                                                                        }
                                                                                                        else{
                                                                                                            console.log("filenames.txt read successfully")
                                                                                                            let filenames = data.toString().split("\n")
                                                                                                            for(let index=0; index < filenames.length; index++){
                                                                                                                fs.unlink(path.join(__dirname,filenames[index]),(err)=>{
                                                                                                                    if(err){
                                                                                                                        console.error(`Deleting file ${filenames[index]} Error`,err)
                                                                                                                    }
                                                                                                                    else{
                                                                                                                        console.log(`File ${filenames[index]} deleted successfully`)
                                                                                                                    }
                                                                                                                })
                                                                                                                
                                                                                                            }
                                                                                                        }
                                                                                                    })
                                                                                                }
                                                                                            })
                                                                                        }
                                                                                    })
                                                                                }
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    })
}

module.exports = fileDataManipulation